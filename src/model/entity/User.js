class User {
    constructor(obj = {
        id: null,
        first_name: "",
        second_name: "",
        patronymic: null,
        city: -1,
        phone: "",
        email: "",
        school: -1,
        year: -1,
        vk: null,
        facebook: null,
        telegram: null,
        whatsapp: null,
        photo: null
    }) {
        this.id = obj.id;
        this.first_name = obj.first_name;
        this.second_name = obj.second_name;
        this.patronymic = obj.patronymic;
        this.city = obj.city;
        this.phone = obj.phone;
        this.email = obj.email;
        this.school = obj.school;
        this.year = obj.year;
        this.vk = obj.vk;
        this.facebook = obj.facebook;
        this.telegram = obj.telegram;
        this.whatsapp = obj.whatsapp;
        this.photo = obj.photo;
    }
}

export default User;
