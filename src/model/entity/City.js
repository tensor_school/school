class City {
    constructor(obj = {
        id: null,
        name: ""
    }) {
        this.id = obj.id;
        this.name = obj.name;
    }
}

export default City;
