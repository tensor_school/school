class School {
    constructor(obj = {
        id: null,
        name: "",
        city: null
    }) {
        this.id = obj.id;
        this.name = obj.name;
        this.city = obj.city;
    }
}

export default School;
