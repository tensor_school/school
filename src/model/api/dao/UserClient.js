import User from "../../entity/User";

class UserClient {
    constructor(apiEndpoint) {
        this.userEndpoint = apiEndpoint + "/user";
    }

    async readAll() {
        const response = await fetch(
            this.userEndpoint,
            {
                method: "GET",
                headers: { "Accept": "application/json" }
            }
        );
        if (response.ok === true) {
            const elements = await response.json()
            return elements.map((obj) => new User(obj));
        }
    }

    async read(id) {
        const response = await fetch(
            this.userEndpoint + "/" + id,
            {
                method: "GET",
                headers: { "Accept": "application/json" }
            }
        );
        if (response.ok === true) {
            const element = await response.json();
            return new User(element);
        }
    }

    async create(user) {
        const response = await fetch(
            this.userEndpoint,
            {
                method: "POST",
                headers: { "Accept": "application/json", "Content-Type": "application/json" },
                body: JSON.stringify(user)
            }
        );
        if (response.ok === true) {
            return await response.json();
        }
    }

    async update(user) {
        const id = user.id;
        const response = await fetch(
            this.userEndpoint + "/" + id,
            {
                method: "PUT",
                headers: { "Accept": "application/json", "Content-Type": "application/json" },
                body: JSON.stringify(user)
            }
        );
        return response.ok === true
    }

    async delete(id) {
        const response = await fetch(
            this.userEndpoint + "/" + id,
            {
                method: "DELETE",
                headers: { "Accept": "application/json", "Content-Type": "application/json" }
            }
        );
        return response.ok === true
    }
}

export default UserClient;
