import School from "../../entity/School";

class SchoolClient {
    constructor(apiEndpoint) {
        this.schoolEndpoint = apiEndpoint + "/school";
    }

    async readAll() {
        const response = await fetch(
            this.schoolEndpoint,
            {
                method: "GET",
                headers: { "Accept": "application/json" }
            }
        );
        if (response.ok === true) {
            const elements = await response.json()
            return elements.map((obj) => new School(obj));
        }
    }

    async read(id) {
        const response = await fetch(
            this.schoolEndpoint + "/" + id,
            {
                method: "GET",
                headers: { "Accept": "application/json" }
            }
        );
        if (response.ok === true) {
            const element = await response.json();
            return new School(element);
        }
    }

    async create(school) {
        const response = await fetch(
            this.schoolEndpoint,
            {
                method: "POST",
                headers: { "Accept": "application/json", "Content-Type": "application/json" },
                body: JSON.stringify(school)
            }
        );
        if (response.ok === true) {
            return await response.json();
        }
    }

    async update(school) {
        const id = school.id;
        const response = await fetch(
            this.schoolEndpoint + "/" + id,
            {
                method: "PUT",
                headers: { "Accept": "application/json", "Content-Type": "application/json" },
                body: JSON.stringify(school)
            }
        );
        return response.ok === true
    }

    async delete(id) {
        const response = await fetch(
            this.schoolEndpoint + "/" + id,
            {
                method: "DELETE",
                headers: { "Accept": "application/json", "Content-Type": "application/json" }
            }
        );
        return response.ok === true
    }
}

export default SchoolClient;
