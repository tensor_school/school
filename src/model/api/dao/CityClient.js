import City from "../../entity/City";

class CityClient {
    constructor(apiEndpoint) {
        this.cityEndpoint = apiEndpoint + "/city";
    }

    async readAll() {
        const response = await fetch(
            this.cityEndpoint,
            {
                method: "GET",
                headers: { "Accept": "application/json" }
            }
        );
        if (response.ok === true) {
            const elements = await response.json()
            return elements.map((obj) => new City(obj));
        }
    }

    async read(id) {
        const response = await fetch(
            this.cityEndpoint + "/" + id,
            {
                method: "GET",
                headers: { "Accept": "application/json" }
            }
        );
        if (response.ok === true) {
            const element = await response.json();
            return new City(element);
        }
    }

    async create(city) {
        const response = await fetch(
            this.cityEndpoint,
            {
                method: "POST",
                headers: { "Accept": "application/json", "Content-Type": "application/json" },
                body: JSON.stringify(city)
            }
        );
        if (response.ok === true) {
            return await response.json();
        }
    }

    async update(city) {
        const id = city.id;
        const response = await fetch(
            this.cityEndpoint + "/" + id,
            {
                method: "PUT",
                headers: { "Accept": "application/json", "Content-Type": "application/json" },
                body: JSON.stringify(city)
            }
        );
        return response.ok === true
    }

    async delete(id) {
        const response = await fetch(
            this.cityEndpoint + "/" + id,
            {
                method: "DELETE",
                headers: { "Accept": "application/json", "Content-Type": "application/json" }
            }
        );
        return response.ok === true
    }
}

export default CityClient;
