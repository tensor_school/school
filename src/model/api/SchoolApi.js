import CityClient from "./dao/CityClient";
import SchoolClient from "./dao/SchoolClient";
import UserClient from "./dao/UserClient";

class SchoolApi {
    constructor() {
        // TODO: Move to `.env`
        const apiEndpoint = "https://school.armacoty.tk/api";
        this.city = new CityClient(apiEndpoint);
        this.school = new SchoolClient(apiEndpoint);
        this.user = new UserClient(apiEndpoint);
    }
}

export default SchoolApi;
