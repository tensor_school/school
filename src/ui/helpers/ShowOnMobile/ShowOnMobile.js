import './ShowOnMobile.css';

import React from 'react';

class ShowOnMobile extends React.Component {
    render() {
        return (
            <div className="show-on-mobile">
                {this.props.children}
            </div>
        );
    }
}

export default ShowOnMobile;