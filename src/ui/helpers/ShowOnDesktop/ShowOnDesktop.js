import './ShowOnDesktop.css';

import React from 'react';

class ShowOnDesktop extends React.Component {
    render() {
        return (
            <div className="show-on-desktop">
                {this.props.children}
            </div>
        );
    }
}

export default ShowOnDesktop;
