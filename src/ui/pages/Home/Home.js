import './Home.css';
import React from 'react';
import NavBar from '../../widgets/NavBar/NavBar';
import CenterContainer from '../../widgets/CenterContainer/CenterContainer';
import MenuCard from '../../widgets/MenuCard/MenuCard';
import classmates from "../../../asset/classmates.png";
import lessons from "../../../asset/lessons.png";
import { Link } from 'react-router-dom';
import DesktopSchoolLogo from '../../widgets/DesktopSchoolLogo/DesktopSchoolLogo';

class Home extends React.Component {
    render() {
        return (
            <div>
                <NavBar isHome={true} />

                <CenterContainer>
                    <DesktopSchoolLogo />

                    <div id="section-holder">
                        <div className='card-wrapper'>
                            <Link to="/classmates">
                                <MenuCard image={classmates} text={"Мои одноклассники"} />
                            </Link>
                        </div>
                        <div className='card-wrapper'>
                            <Link to="/home">
                                <MenuCard image={lessons} text={"Темы уроков"} />
                            </Link>
                        </div>
                    </div>
                </CenterContainer>
            </div>
        );
    }
}

export default Home;
