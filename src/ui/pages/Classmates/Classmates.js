import './Classmates.css';

import React from 'react';

import CenterContainer from '../../widgets/CenterContainer/CenterContainer';
import NavBar from '../../widgets/NavBar/NavBar';
import SchoolApi from '../../../model/api/SchoolApi';
import UserItem from '../../widgets/UserItem/UserItem';
import DesktopSchoolLogo from '../../widgets/DesktopSchoolLogo/DesktopSchoolLogo';
import ShowOnMobile from '../../helpers/ShowOnMobile/ShowOnMobile';
import UserCard from '../../widgets/UserCard/UserCard';
import ShowOnDesktop from '../../helpers/ShowOnDesktop/ShowOnDesktop';
import BlueButton from '../../widgets/BlueButton/BlueButton';
import { Link } from 'react-router-dom';

class Classmates extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            data: [],
            activeUser: null
        };
    }

    mouseX = 0;
    mouseY = 0;

    componentDidMount() {
        const client = new SchoolApi();
        client.user
            .readAll()
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        data: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error: error
                    });
                }
            );
        document.addEventListener('mousemove', this.onMouseUpdate, false);
        document.addEventListener('mouseenter', this.onMouseUpdate, false);
    }

    onMouseUpdate = (event) => {
        this.mouseX = event.pageX;
        this.mouseY = event.pageY;
    }

    onDeleteClick = (user) => {
        this.setState({
            data: this.state.data.filter((value) => (value !== user)),
        });
        const client = new SchoolApi();
        client.user.delete(user.id);
    }

    on = (user) => {
        this.setState({
            activeUser: user
        });
    }

    off = () => {
        this.setState({
            activeUser: null
        });
    }

    render() {
        let usersList;
        if (this.state.isLoaded) {
            usersList = (
                <div>
                    {this.state.data.map((user) => (
                        <div className='users-list-item' key={user.id}>
                            <UserItem user={user} onDeleteClick={this.onDeleteClick} onUserClick={() => { this.on(user); }} />
                        </div>
                    ))}
                </div>
            );
        } else {
            usersList = (<div></div>);
        }

        return (
            <div>
                <ShowOnMobile>
                    <NavBar addButton={true} />
                </ShowOnMobile>

                <CenterContainer>
                    <DesktopSchoolLogo />

                    <ShowOnDesktop>
                        <Link to="/adduser">
                            <BlueButton>
                                Добавить
                            </BlueButton>
                        </Link>
                    </ShowOnDesktop>

                    {usersList}
                </CenterContainer>

                {this.state.activeUser ?
                    <div
                        id='overlay'
                        onClick={this.off}
                        style={{
                            paddingLeft: this.mouseX + "px",
                            paddingTop: this.mouseY + "px"
                        }}>
                        <UserCard user={this.state.activeUser} />
                    </div> :
                    <div></div>
                }
            </div>
        );
    }
}

export default Classmates;
