import './Start.css';

import React from 'react';
import { Link } from 'react-router-dom';
import SchoolLogo from '../../widgets/SchoolLogo/SchoolLogo';
import startPageImage from '../../../asset/StartPageImage.png';

class Start extends React.Component {
    render() {
        const text1 = "Это страница школы Тензор.";
        const text2 = "Тут вы можете познакомиться с нашими учениками и";
        const text3 = "посмотреть темы занятий.";

        return (
            <Link to="/home">
                <div className="start-page">
                    <SchoolLogo />
                    <img src={startPageImage} alt="" />
                    <p>
                        {text1}
                        <br />
                        {text2}
                        <br />
                        {text3}
                    </p>
                </div>
            </Link>
        );
    }
}

export default Start;