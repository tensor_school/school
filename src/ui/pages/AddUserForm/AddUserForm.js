import './AddUserForm.css';

import React from 'react';
import NavBar from '../../widgets/NavBar/NavBar';
import CenterContainer from '../../widgets/CenterContainer/CenterContainer';
import DesktopSchoolLogo from '../../widgets/DesktopSchoolLogo/DesktopSchoolLogo';
import SchoolApi from '../../../model/api/SchoolApi';
import User from '../../../model/entity/User';
import BlueButton from '../../widgets/BlueButton/BlueButton';

class AddUserForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            school: {
                error: null,
                isLoaded: false,
                data: []
            },
            city: {
                error: null,
                isLoaded: false,
                data: []
            },
            validationError: null
        };
    }

    componentDidMount() {
        const client = new SchoolApi();
        client.city
            .readAll()
            .then(
                (result) => {
                    this.setState({
                        city: {
                            isLoaded: true,
                            data: result
                        }
                    });
                },
                (error) => {
                    this.setState({
                        city: {
                            isLoaded: true,
                            error: error
                        }
                    });
                }
            );
        client.school
            .readAll()
            .then(
                (result) => {
                    this.setState({
                        school: {
                            isLoaded: true,
                            data: result
                        }
                    });
                },
                (error) => {
                    this.setState({
                        school: {
                            isLoaded: true,
                            error: error
                        }
                    });
                }
            );
    }

    onSubmit = () => {
        const emptyStringToNull = (str) => {
            if (str === "") {
                return null;
            } else {
                return str;
            }
        }

        const validateEmail = (email) => {
            return String(email)
                .toLowerCase()
                .match(
                    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                );
        };


        const validatePhone = (phone) => {
            return String(phone)
                .toLowerCase()
                .match(
                    /^([+]?[\s0-9]+)?(\d{3}|[(]?[0-9]+[)])?([-]?[\s]?[0-9])+$/
                );
        };

        this.setState({
            validationError: null
        });

        const second_name_element = document.querySelector("#second_name");
        const second_name = second_name_element.value;
        if (second_name === "") {
            this.setState({
                validationError: "Поле 'Фамилия' должно быть заполнено"
            });
            return;
        }

        const first_name_element = document.querySelector("#first_name");
        const first_name = first_name_element.value;
        if (first_name === "") {
            this.setState({
                validationError: "Поле 'Имя' должно быть заполнено"
            });
            return;
        }

        const patronymic_element = document.querySelector("#patronymic");
        const patronymic = patronymic_element.value;

        const city_element = document.querySelector("#city");
        const city = +city_element.value;

        const phone_element = document.querySelector("#phone");
        const phone = phone_element.value;
        if (phone === "") {
            this.setState({
                validationError: "Поле 'Телефон' должно быть заполнено"
            });
            return;
        }
        if (!validatePhone(phone)) {
            this.setState({
                validationError: "Поле 'Телефон' заполнено неверно"
            });
            return;
        }

        const email_element = document.querySelector("#email");
        const email = email_element.value;
        if (email === "") {
            this.setState({
                validationError: "Поле 'Почта' должно быть заполнено"
            });
            return;
        }
        if (!validateEmail(email)) {
            this.setState({
                validationError: "Поле 'Почта' заполнено неверно"
            });
            return;
        }

        const school_element = document.querySelector("#school");
        const school = +school_element.value;

        const year_element = document.querySelector("#year");
        const year = +year_element.value;

        const vk_element = document.querySelector("#vk");
        const vk = vk_element.value;

        const facebook_element = document.querySelector("#facebook");
        const facebook = facebook_element.value;

        const telegram_element = document.querySelector("#telegram");
        const telegram = telegram_element.value;

        const whatsapp_element = document.querySelector("#whatsapp");
        const whatsapp = whatsapp_element.value;

        const photo_element = document.querySelector("#photo");
        const photo = photo_element.value;

        const user = new User({
            first_name: first_name,
            second_name: second_name,
            patronymic: emptyStringToNull(patronymic),
            city: city,
            phone: phone,
            email: email,
            school: school,
            year: year,
            vk: emptyStringToNull(vk),
            facebook: emptyStringToNull(facebook),
            telegram: emptyStringToNull(telegram),
            whatsapp: emptyStringToNull(whatsapp),
            photo: emptyStringToNull(photo)
        });

        const client = new SchoolApi();
        client.user.create(user).then(
            (result) => {
                window.location.replace('/classmates');
            },
            (error) => { }
        );
    }

    render() {
        let requiredMark = (<span style={{ color: "red" }}>*</span>);

        return (
            <div>
                <NavBar isHome={false} />
                <CenterContainer>
                    <DesktopSchoolLogo />
                    <form>
                        <div className='form'>
                            <div className='form-row'>
                                <div className='form-left-cell'>
                                    <label htmlFor="second_name">{requiredMark}Фамилия:</label>
                                </div>
                                <div className='form-right-cell'>
                                    <input name="second_name" id="second_name" />
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-left-cell'>
                                    <label htmlFor='first_name'>{requiredMark}Имя:</label>
                                </div>
                                <div className='form-right-cell'>
                                    <input name="first_name" id="first_name" />
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-left-cell'>
                                    <label htmlFor="patronymic">Отчество:</label>
                                </div>
                                <div className='form-right-cell'>
                                    <input name="patronymic" id="patronymic" />
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-left-cell'>
                                    <label htmlFor="city">{requiredMark}Город:</label>
                                </div>
                                <div className='form-right-cell'>
                                    <select name="city" id="city" >
                                        {
                                            this.state.city.data.map((city) => {
                                                return (<option value={city.id} key={city.id}>{city.name}</option>);
                                            })
                                        }
                                    </select>
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-left-cell'>
                                    <label htmlFor="phone">{requiredMark}Телефон:</label>
                                </div>
                                <div className='form-right-cell'>
                                    <input name="phone" id="phone" />
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-left-cell'>
                                    <label htmlFor="email">{requiredMark}Почта:</label>
                                </div>
                                <div className='form-right-cell'>
                                    <input name="email" id="email" />
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-left-cell'>
                                    <label htmlFor="school">{requiredMark}ВУЗ:</label>
                                </div>
                                <div className='form-right-cell'>
                                    <select name="school" id="school" >
                                        {
                                            this.state.school.data.map((school) => {
                                                return (<option value={school.id} key={school.id}>{school.name}</option>);
                                            })
                                        }
                                    </select>
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-left-cell'>
                                    <label htmlFor="year">{requiredMark}Курс:</label>
                                </div>
                                <div className='form-right-cell'>
                                    <select name="year" id="year">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-left-cell'>
                                    <label htmlFor="vk">VK:</label>
                                </div>
                                <div className='form-right-cell'>
                                    <input name="vk" id="vk" />
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-left-cell'>
                                    <label htmlFor="facebook">Facebook:</label>
                                </div>
                                <div className='form-right-cell'>
                                    <input name="facebook" id="facebook" />
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-left-cell'>
                                    <label htmlFor="telegram">Telegram:</label>
                                </div>
                                <div className='form-right-cell'>
                                    <input name="telegram" id="telegram" />
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-left-cell'>
                                    <label htmlFor="whatsapp">Whatsapp:</label>
                                </div>
                                <div className='form-right-cell'>
                                    <input name="whatsapp" id="whatsapp" />
                                </div>
                            </div>
                            <div className='form-row'>
                                <div className='form-left-cell'>
                                    <label htmlFor="photo">Ссылка на фото:</label>
                                </div>
                                <div className='form-right-cell'>
                                    <input name="photo" id="photo" />
                                </div>
                            </div>
                        </div>
                        <p style={{ color: "red" }}>{this.state.validationError}</p>
                    </form>

                    <div onClick={this.onSubmit}>
                        <BlueButton>
                            Создать
                        </BlueButton>
                    </div>
                </CenterContainer>
            </div>
        );
    }
}

export default AddUserForm;