import './DesktopSchoolLogo.css';

import React from 'react';
import SchoolLogo from '../SchoolLogo/SchoolLogo';
import ShowOnDesktop from '../../helpers/ShowOnDesktop/ShowOnDesktop';
import { Link } from 'react-router-dom';

class DesktopSchoolLogo extends React.Component {
    render() {
        return (
            <ShowOnDesktop>
                <Link to="/home">
                    <div className='desktop-school-logo'>
                        <SchoolLogo horizontal={true} />
                    </div>
                </Link>
            </ShowOnDesktop>
        );
    }
}

export default DesktopSchoolLogo;
