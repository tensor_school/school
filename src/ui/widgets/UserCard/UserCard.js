import './UserCard.css';

import React from 'react';

import icGeo from '../../../asset/ic_geo.svg';
import icSchool from '../../../asset/ic_school.svg';
import icPhone from '../../../asset/ic_phone.svg';
import icMail from '../../../asset/ic_mail.svg';

import icVk from "../../../asset/ic_vk.svg";
import icFb from "../../../asset/ic_fb.svg";
import icWa from "../../../asset/ic_wa.svg";
import icTg from "../../../asset/ic_tg.svg";
import SchoolApi from '../../../model/api/SchoolApi';

class UserCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            school: {
                error: null,
                isLoaded: false,
                data: {}
            },
            city: {
                error: null,
                isLoaded: false,
                data: {}
            }
        };
    }

    componentDidMount() {
        if (this.props.user) {
            const client = new SchoolApi();
            client.city
                .read(this.props.user.city)
                .then(
                    (result) => {
                        this.setState({
                            city: {
                                isLoaded: true,
                                data: result
                            }
                        });
                    },
                    (error) => {
                        this.setState({
                            city: {
                                isLoaded: true,
                                error: error
                            }
                        });
                    }
                );
            client.school
                .read(this.props.user.school)
                .then(
                    (result) => {
                        this.setState({
                            school: {
                                isLoaded: true,
                                data: result
                            }
                        });
                    },
                    (error) => {
                        this.setState({
                            school: {
                                isLoaded: true,
                                error: error
                            }
                        });
                    }
                );
        }
    }

    render() {
        let user = this.props.user;
        let cityText = (<p></p>);
        if (this.state.city.isLoaded) {
            cityText = (<p>{this.state.city.data.name}</p>);
        }

        let schoolText = (<p></p>);
        if (this.state.school.isLoaded) {
            schoolText = (<p>{this.state.school.data.name}, {user.year} курс</p>);
        }

        return (
            <div className='user-card'>
                <div className='user-card-left-col'>
                    <div className='user-card-name'>
                        {user.second_name.toUpperCase()}
                        <br />
                        {user.first_name + " "}
                        {user.patronymic}
                    </div>

                    <div className='user-card-info'>
                        <div className='user-card-info-item'>
                            <img src={icGeo} alt="" />
                            {cityText}
                        </div>
                        <div className='user-card-info-item'>
                            <img src={icSchool} alt="" />
                            {schoolText}
                        </div>
                        <div className='user-card-info-item'>
                            <img src={icPhone} alt="" />
                            <a href={"tel:" + user.phone}>
                                <p>{user.phone + ""}</p>
                            </a>
                        </div>
                        <div className='user-card-info-item'>
                            <img src={icMail} alt="" />
                            <a href={"mailto:" + user.phone}>
                                <p>{user.email + ""}</p>
                            </a>
                        </div>
                    </div>
                </div>
                <div className='user-card-right-col'>
                    <img src={user.photo} alt="" />

                    <div className='user-card-socials'>
                        {user.vk ?
                            <a href={user.vk}>
                                <img src={icVk} className="user-card-social-active" alt="" />
                            </a> :
                            <img src={icVk} className="user-card-social-disabled" alt="" />
                        }
                        {user.facebook ?
                            <a href={user.facebook}>
                                <img src={icFb} className="user-card-social-active" alt="" />
                            </a> :
                            <img src={icFb} className="user-card-social-disabled" alt="" />
                        }
                        {user.whatsapp ?
                            <a href={user.whatsapp}>
                                <img src={icWa} className="user-card-social-active" alt="" />
                            </a> :
                            <img src={icWa} className="user-card-social-disabled" alt="" />
                        }
                        {user.telegram ?
                            <a href={user.telegram}>
                                <img src={icTg} className="user-card-social-active" alt="" />
                            </a> :
                            <img src={icTg} className="user-card-social-disabled" alt="" />
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default UserCard;
