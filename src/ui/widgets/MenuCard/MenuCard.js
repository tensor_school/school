import './MenuCard.css';
import React from 'react';

class MenuCard extends React.Component {
    render() {
        return (
            <div className='menu-card'>
                <img className='menu-card-image' src={this.props.image} alt="oops" />
                <div className='menu-card-text'>
                    <p>{this.props.text}</p>
                </div>
            </div>
        );
    }
}

export default MenuCard;