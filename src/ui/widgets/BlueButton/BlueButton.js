import './BlueButton.css';

import React from 'react';

class BlueButton extends React.Component {
    render() {
        return (
            <div className="blue-button">
                {this.props.children}
            </div>
        );
    }
}

export default BlueButton;