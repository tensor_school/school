import React from 'react';
import './SchoolLogo.css';

import logo from "../../../asset/TensorLogo.png";

class SchoolLogo extends React.Component {
    render() {
        if (this.props.horizontal === true) {
            return (
                <div id="school-logo-horizontal">
                    <img src={logo} alt="TENSOR-LOGO" />
                    <div id="school-logo-horizontal-text">TENSOR SCHOOL</div>
                </div>
            );
        } else {
            return (
                <div id="school-logo">
                    <img src={logo} alt="TENSOR-LOGO" />
                    <div id="school-logo-text">TENSOR SCHOOL</div>
                </div>
            );
        }
    }
}

export default SchoolLogo;
