import './UserItem.css';

import React from 'react';
import SchoolApi from '../../../model/api/SchoolApi';
import deleteIcon from '../../../asset/ic_delete.svg';

class UserItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            school: {
                error: null,
                isLoaded: false,
                data: {}
            },
            city: {
                error: null,
                isLoaded: false,
                data: {}
            }
        };
    }

    componentDidMount() {
        const client = new SchoolApi();
        client.city
            .read(this.props.user.city)
            .then(
                (result) => {
                    this.setState({
                        city: {
                            isLoaded: true,
                            data: result
                        }
                    });
                },
                (error) => {
                    this.setState({
                        city: {
                            isLoaded: true,
                            error: error
                        }
                    });
                }
            );
        client.school
            .read(this.props.user.school)
            .then(
                (result) => {
                    this.setState({
                        school: {
                            isLoaded: true,
                            data: result
                        }
                    });
                },
                (error) => {
                    this.setState({
                        school: {
                            isLoaded: true,
                            error: error
                        }
                    });
                }
            );
    }

    onDeleteClick = () => {
        this.props.onDeleteClick(this.props.user);
    }

    render() {
        let user = this.props.user;

        let cityName = "";
        if (this.state.city.isLoaded) {
            cityName = this.state.city.data.name;
        }

        let schoolName = "";
        if (this.state.school.isLoaded) {
            schoolName = this.state.school.data.name;
        }

        return (
            <div className='user-item'>
                <img className='user-item-avatar' src={user.photo} alt="" onClick={this.props.onUserClick} />
                <div className='user-item-text-column' onClick={this.props.onUserClick}>
                    <div className='user-item-name'>
                        {user.first_name} {user.second_name}
                    </div>
                    <div className='user-item-school'>
                        {schoolName} {user.year} курс{"\u2001"}{cityName}
                    </div>
                </div>
                <div className="user-item-delete" onClick={this.onDeleteClick}>
                    <img className='user-item-delete-icon' src={deleteIcon} alt="" />
                </div>
            </div>
        );
    }
}

export default UserItem;