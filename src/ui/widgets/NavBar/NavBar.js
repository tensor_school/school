import React from 'react';
import { Link } from 'react-router-dom';
import ShowOnMobile from '../../helpers/ShowOnMobile/ShowOnMobile';
import './NavBar.css';

class NavBar extends React.Component {
    render() {
        let icon;

        if (this.props.isHome) {
            icon = (
                <div id="navbar-icon">
                    <i className="material-icons">menu</i>
                </div>
            );
        } else {
            icon = (
                <Link to="/home">
                    <div id="navbar-icon">
                        <i className="material-icons">arrow_back</i>
                    </div>
                </Link>
            );
        }

        let addButton = (<div></div>);
        if (this.props.addButton) {
            addButton = (
                <Link to="/adduser">
                    <div id="navbar-action">
                        <i className="material-icons">add</i>
                    </div>
                </Link>
            );
        }

        return (
            <ShowOnMobile>
                <div id="navbar">
                    {icon}
                    <div id="navbar-title">
                        TENSOR SCHOOL
                    </div>
                    {addButton}
                </div>
            </ShowOnMobile>
        );
    }
}

export default NavBar;
