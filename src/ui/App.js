import './App.css';

import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";

import Home from './pages/Home/Home';
import Classmates from './pages/Classmates/Classmates';
import AddUserForm from './pages/AddUserForm/AddUserForm';
import Start from './pages/Start/Start';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Start />} />
        <Route path="/home" element={<Home />} />
        <Route path="/classmates" element={<Classmates />} />
        <Route path="/adduser" element={<AddUserForm />} />
      </Routes>
    </Router>
  );
}

export default App;
