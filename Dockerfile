FROM node:lts AS builder
WORKDIR /root/school
COPY package.json ./
RUN npm install --only=prod
COPY . .
RUN npm run build

FROM nginx:alpine
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /root/school/build /usr/share/nginx/html/
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
